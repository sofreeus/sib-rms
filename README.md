# A Tribute to Richard Matthew Stallman

First delivery on the 66th anniversary of his birth.

Objectives:

What is free software?

What did RMS make? (7 things)

## Talk (15 minutes)

"I became part of a software-sharing community that had existed for many years. Sharing of software was not limited to our particular community; it is as old as computers, just as sharing of recipes is as old as cooking." --Richard M. Stallman

![Richard M. Stallman](richard-stallman.jpg)

- 1976 - Bill Gates' "Open Letter to Hobbyists"

    "Who can afford to do professional work for nothing? What hobbyist can put 3-man years into programming, finding all bugs, documenting his product and distribute for free? The fact is, no one besides us has invested a lot of money in hobby software." --Bill Gates

- 1980 - The un-free Xerox printer driver problem

- 1984 - resigns in protest, starts GNU project

    [RMS as St. Ignucious](ignucious.jpeg)
    
    GNU Manifesto: "I consider that the Golden Rule requires that if I like a program I must share it with other people who like it."
    
    GNU General Public License: "The GNU General Public License is a free, copyleft license for software and other kinds of works.
    
    "... is intended to guarantee your freedom to share and change all versions of a program--to make sure it remains free software for all its users.
    
    ...
    
    "When we speak of free software, we are referring to freedom, not price."

- 1984 - Hackers by Steven Levy

- 1985 - emacs (on tape: $150)
- 1986 - gdb
- 1987 - gcc v. 0.9

- 1991 - Linus' email / birth of Linux

    "I am doing a (free) operating system (just a hobby, wonât be big and professional like gnu) ...
    "I've currently ported bash (1.08) and gcc (1.40)..." --Linus Torvalds.

- 1997 - Miguel de Icaza's GNOME

- 1998 - ESR's "Open Source" software initiative

- 2002 - RMS' "Free Software, Free Society"

- 2002 - Sam Williams' "Free As In Freedom"


## Demo (15 minutes)


```bash
# fedora:
sudo dnf install emacs-nox gcc gdb

# ubuntu:
sudo apt update && sudo apt install emacs-nox gcc gdb

# opensuse:
sudo zypper install emacs-nox gcc gdb
```

1. Start emacs
2. Write hello-world.c
3. Compile it with gcc

## Pair and Share (30 minutes)

Do likewise, in pairs. Take turns in expert/novice. Go as fast as you can. As soon as your pair is done, whoop loudly, and do an airborn belly bump.

Share:

What are the most important contributions RMS has made to the free software movement?
